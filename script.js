const html = document.querySelector("html");

html.addEventListener("dblclick", (e) => {
    const bell = document.getElementById("bell-icon");
    const notification = document.getElementById("notification-placeholder");

    notification.classList.remove("notification-circle");
    bell.classList.remove("bell-animation");

    notification.offsetHeight;
    bell.offsetHeight;

    notification.classList.add("notification-circle");
    bell.classList.add("bell-animation");
});

function closeModal(modal_id) {
    var modal_el = document.getElementById(modal_id);
    var modal = bootstrap.Modal.getInstance(modal_el)
    modal.hide();
}

function showModal(modal_id) {
    var modal = new bootstrap.Modal(document.getElementById(modal_id));
    modal.show();
}

function addStudent() {
    var group = document.getElementById("student-group");
    var first_name = document.getElementById("student-first-name");
    var last_name = document.getElementById("student-last-name");
    var gender = document.getElementById("student-gender");
    var birthday = document.getElementById("student-birthday");

    var table = document.getElementById("student-table");
    var row = table.insertRow(-1);

    var cell1 = row.insertCell(-1);
    cell1.innerHTML = "<input type=\"checkbox\">";

    var cell2 = row.insertCell(-1);
    cell2.innerHTML = group.value;

    var cell3 = row.insertCell(-1);
    cell3.innerHTML = first_name.value + " " + last_name.value;

    var cell4 = row.insertCell(-1);
    cell4.innerHTML = gender.value;

    var cell5 = row.insertCell(-1);
    cell5.innerHTML = birthday.value;

    var cell6 = row.insertCell(-1);
    cell6.innerHTML = "<span class=\"gray-dot\" title=\"offline\"></span>";

    var cell7 = row.insertCell(-1);
    cell7.innerHTML =  `<button type="button" class="table-button btn btn-square-md" onclick="editStudent(this)">
                            <i class="fas fa-pencil"></i>
                        </button>
                        <button type="button" class="table-button btn btn-square-md" onclick="deleteStudentModal(this)">
                            <i class="fa-solid fa-xmark"></i>
                        </button>`;

    group.value = "";
    first_name.value = "";
    last_name.value = "";
    gender.value = "";
    birthday.value = "";

    closeModal("modal-add-student");
}

function deleteStudentModal(element) {
    showModal("modal-delete-student");

    var row = element.parentElement.parentElement;
    var name = row.cells[2].textContent;

    var student_name = document.getElementById("delete-student-name");
    student_name.innerHTML = name;

    var button = document.getElementById("delete-student-button");

    button.onclick = function deleteRow() {
        var table = row.parentElement;
        table.deleteRow(row.rowIndex - 1);
        closeModal("modal-delete-student");
    }
}

function editStudent(element) {
    showModal("modal-edit-student");
    var row = element.parentElement.parentElement;

    var group = document.getElementById("edit-student-group");
    var first_name = document.getElementById("edit-student-first-name");
    var last_name = document.getElementById("edit-student-last-name");
    var gender = document.getElementById("edit-student-gender");
    var birthday = document.getElementById("edit-student-birthday");

    group.value = row.cells[1].textContent;
    first_name.value = row.cells[2].textContent.split(" ")[0];
    last_name.value = row.cells[2].textContent.split(" ")[1];
    gender.value = row.cells[3].textContent;
    birthday.value = row.cells[4].textContent;

    var button = document.getElementById("edit-student-button");

    button.onclick = function editRow() {
        row.cells[1].textContent = group.value;
        row.cells[2].textContent = first_name.value + " " + last_name.value;
        row.cells[3].textContent = gender.value;
        row.cells[4].textContent = birthday.value;

        closeModal("modal-edit-student");
    }
}
